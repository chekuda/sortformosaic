const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors())

app.get('/', (req, res) => res.sendFile(__dirname + '/rates.json'))

app.listen(8081)

console.log('SERVER DONE')
