import React, { Component } from 'react'

import './App.scss'

class App extends Component {
  state = {
    sortRates: 0,
    sortNumber: 0,
    rates: []
  }

  componentDidMount() {
    fetch('http://localhost:8081')
      .then(res => res.json())
      .then(data => {
        this.setState({
          rates: Object.entries(data.rates).reduce((acc, [name, value]) => acc.concat({ name, value }), [])
        })
      })
  }


  sortData(type) {
    const { rates } = this.state

    const objHelper = {
      sortRates: 'name',
      sortNumber: 'value'
    }

    this.setState({
      rates: this.state[type] % 2 !== 0
        ? rates.sort((first, second) => first[objHelper[type]] > second[objHelper[type]] ? 1 : -1)
        : rates.sort((first, second) => first[objHelper[type]] > second[objHelper[type]] ? -1 : 1)
    })
  }

  onClickRates = type => {
    const { sortRates, sortNumber } = this.state

    this.setState({
      sortRates: type === 'sortRates' ? sortRates + 1 : 0,
      sortNumber: type === 'sortNumber' ? sortNumber + 1 : 0
    }, () => this.sortData(type))
  }

  render() {
    return <div>
      <table>
        <tbody>
        <tr>
          <td onClick={() => this.onClickRates('sortRates')}>
            Rates
          </td>
          <td onClick={() => this.onClickRates('sortNumber')}>
            Number
          </td>
        </tr>
        {
          this.state.rates.map(({name, value}) => (
            <tr key={name}>
              <td>
                {name}
              </td>
              <td>
                {value}
              </td>
            </tr>
          ))
        }
        </tbody>
      </table>
    </div>
  }
}

export default App
